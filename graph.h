#pragma once
#include "Node.h"
#include "edge.h"
#include<list>
using namespace std;
class graph {
public:
	bool checkmulti();
	bool checkpseudo(list<Edge>);
	bool checkDigraph(list<Edge> );
	bool checkWight();
	bool checkComplete(list <Edge>);
private:
	list<Node> nodes;
	list<Edge> edges;
};

bool graph::checkmulti() {
	return false;	//can't be a multi graph since the array can store only one value for each [i,j] position 
}

bool graph::checkDigraph(list<Edge> edges) {
	bool isDigraph = false;
	list<Edge>::const_iterator iterator;
	list<Edge>::const_iterator temp_iterator;
	for (iterator = edges.begin(); iterator != edges.end();++iterator) {
		for (temp_iterator = edges.begin(); temp_iterator != edges.end(); ++temp_iterator ) {
			if (temp_iterator->getWeight() == iterator->getWeight()) {
				return false;
			}
			else if (iterator->getStart().getName() == temp_iterator->getEnd().getName() && iterator->getEnd().getName() == temp_iterator->getStart().getName() && iterator->getWeight() != temp_iterator->getWeight()) {
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return false;
}

bool graph::checkpseudo(list<Edge> edges) {
	
	list<Edge>::const_iterator iterator;
	for (iterator = edges.begin(); iterator != edges.end(); ++iterator) {
		if (iterator->getStart().getName() == iterator->getEnd().getName() && iterator->getWeight() != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}

bool graph::checkComplete(list <Edge> edges) {
	list<Edge>::const_iterator iterator;
	for (iterator = edges.begin(); iterator != edges.end(); ++iterator) {
		if (iterator->getWeight() == 0) {
			return false;
		}
		else
		{
			return true;
		}
	}
	return true;
}

bool graph::checkWight() {
	return true; //since we define the every weight on the edges of the graph it's pretty obvious that it will
				 //always be weight graph
}