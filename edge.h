#pragma once
#include"Node.h"

class Node;
using namespace std;

class Edge {
public:
	int getWeight() const{ return weight; }
	Node getStart() const{ return start; }
	Node getEnd() const{ return end; }
	void setWeight(int);
	void setStart(Node);
	void setEnd(Node);
private:
	int weight;
	Node start;
	Node end;
};

void Edge::setWeight(int input_Weight) {
	weight = input_Weight;
}

void Edge::setStart(Node input_Start) {
	start = input_Start;
}

void Edge::setEnd(Node input_End) {
	end = input_End;
}